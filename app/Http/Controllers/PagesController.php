<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{

    public function contact(){

      $contact = '09999999999';
      return view('pages.contact',compact('contact'));

    }

    public function about(){

      $abouts = ['About A','About B','About C'];
      return view('pages.about',compact('abouts'));

    }

    public function service(){

      $services = ['Service A','Service B','Service C'];
      return view('pages.service',compact('services'));

    }

}
