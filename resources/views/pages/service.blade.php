
@extends('layouts.app')

@section('title', 'Service Page')

@section('content')
  <h1>Service Page</h1>
  @if (count($services))
    <ul>
      @foreach ($services as $service)
        <li>{{$service}}</li>
      @endforeach
    </ul>
  @endif
@endsection

{{-- @section('script')
  <script type="text/javascript">
    alert('About Page');
  </script>
@endsection --}}
