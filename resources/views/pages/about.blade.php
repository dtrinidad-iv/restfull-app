
@extends('layouts.app')

@section('title', 'About Page')

@section('content')
  <h1>About Page</h1>
  @if (count($abouts))
    <ul>
      @foreach ($abouts as $about)
        <li>{{$about}}</li>
      @endforeach
    </ul>
  @endif
@endsection



{{-- @section('script')
  <script type="text/javascript">
    alert('About Page');
  </script>
@endsection --}}
