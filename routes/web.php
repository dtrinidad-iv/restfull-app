<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// BASIC ROUTING
// Route::get('contact', function()
// {
//     return 'Contact Page';
// });

// Route::get('about', function()
// {
//     return 'About Page';
// });


//ROUTE PARAMETERS
//Required params
Route::get('required-params/{id}/{name}', function($id,$name)
{
    return 'The value of {id} is ' . $id . 'and {name} is ' .$name;
});

//Optional params
Route::get('optional-params/{name?}', function($name='default name')
{
    return 'The value of {name} is ' .$name;
});

//NAMED ROUTES
Route::get('very/very/very/long/named-routes', function()
{
    return route('named-routes');

})->name('named-routes');


// CONTROLLERS
Route::get('contact','PagesController@contact');
Route::get('about','PagesController@about');
Route::get('services','PagesController@service');
Route::resource('resource','ResourcesController');
